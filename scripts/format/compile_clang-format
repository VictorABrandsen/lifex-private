#!/bin/bash
## ---------------------------------------------------------------------
## Copyright (C) 2019 - 2022 by the lifex authors.
##
## This file is part of lifex.
##
## lifex is free software; you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## lifex is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with lifex.  If not, see <http://www.gnu.org/licenses/>.
## ---------------------------------------------------------------------

# Author: Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.

##
## This script has been readapted from the corresponding file
## available at the deal.II development repository
## (https://github.com/dealii/dealii),
## released under compatible license terms
## (please consult the doc/licenses directory for more information).
##

#
# This script downloads, compiles and installs the clang-format binary. The
# destination directory is
#   ./scripts/format/clang/bin.
#
# Compiling clang-format and all necessary parts of LLVM/CLANG might
# require a significant amount of resources. Alternatively, you can use
# download_clang-format to install a statically-linked binary.
#

set -e -u

BASEDIR="$(cd "$(dirname "$0")" && pwd)"

CLANG_VERSION=10.0.0
CLANG_PATH="${BASEDIR}/clang-format-${CLANG_VERSION}"

if [ ! -d "${BASEDIR}" ]
then
    echo "Creating folder ${BASEDIR}."
    mkdir -p "${BASEDIR}"
fi

if [ -d "${CLANG_PATH}" ]
then
    echo "${CLANG_PATH} exists. Exiting."
    exit 1
fi

mkdir -p "${CLANG_PATH}/bin"

LLVM_NAME=llvm-project-${CLANG_VERSION}
URL="https://github.com/llvm/llvm-project/releases/download/llvmorg-${CLANG_VERSION}/${LLVM_NAME}.tar.xz"

echo "Downloading and compiling clang-format-${CLANG_VERSION}."

tmpdir="${TMPDIR:-/tmp}/clang${RANDOM}${RANDOM}"
mkdir -p "${tmpdir}"
cd "${tmpdir}"

if [ -x "$(command -v wget)" ]; then
  echo "Using wget to download..."
  wget "${URL}"
else
  if [ -x "$(command -v curl)" ]; then
    echo "Using curl to download..."
    curl "${URL}" -O
  else
    echo "Error: Neither wget nor curl is available..."
    exit 1
  fi
fi

tar xf ${LLVM_NAME}.tar.xz

mkdir -p ${LLVM_NAME}/build
cd ${LLVM_NAME}/build

case "${OSTYPE}" in
  linux*)
    cmake -DLLVM_ENABLE_PROJECTS=clang \
          -DBUILD_SHARED_LIBS=OFF \
          -DCMAKE_BUILD_TYPE=MinSizeRel \
          -DCMAKE_CXX_FLAGS_MINSIZEREL="-Os -DNDEBUG -static -s" \
          ../llvm
    ;;
  darwin*)
    cmake -DLLVM_ENABLE_PROJECTS=clang \
          -DBUILD_SHARED_LIBS=OFF \
          -DCMAKE_BUILD_TYPE=MinSizeRel \
          -DCMAKE_CXX_FLAGS_MINSIZEREL="-Os -s -DNDEBUG -static-libgcc -static-libstdc++" \
          -DCMAKE_OSX_DEPLOYMENT_TARGET=10.9 \
          ../llvm
    ;;
  *)
    echo "Unknown OS: ${OSTYPE}"
    exit 1
    ;;
esac

make clang-format -j$(nproc)


cp bin/clang-format "${CLANG_PATH}/bin/clang-format"
rm -rf "${tmpdir}"

echo "All done. clang-format-${CLANG_VERSION} successfully installed into"
echo "    ${CLANG_PATH}/bin"
