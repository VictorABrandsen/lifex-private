## ---------------------------------------------------------------------
## Copyright (C) 2021 - 2022 by the lifex authors.
##
## This file is part of lifex.
##
## lifex is free software; you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## lifex is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with lifex.  If not, see <http://www.gnu.org/licenses/>.
## ---------------------------------------------------------------------

# Author: Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.

BASENAME=references

DIR_LATEX=latex
FLAGS_LATEX=-synctex=1 -interaction=nonstopmode --output-directory=$(DIR_LATEX)
SOURCE_LATEX=$(DIR_LATEX)/$(BASENAME).tex
TARGET_LATEX=$(SOURCE_LATEX:.tex=.pdf)

DIR_JABREF=jabref
FLAGS_JABREF=-n -p $(DIR_JABREF)/jabref_config.xml
TARGET_JABREF=$(BASENAME).hpp


.PHONY: all latex $(TARGET_LATEX) jabref $(TARGET_JABREF) clean

all: latex jabref

latex: $(TARGET_LATEX)

$(TARGET_LATEX):
	pdflatex $(FLAGS_LATEX) $(SOURCE_LATEX) || true
	biber --output-directory=$(DIR_LATEX) $(BASENAME)
	pdflatex $(FLAGS_LATEX) $(SOURCE_LATEX) || true
	pdflatex $(FLAGS_LATEX) $(SOURCE_LATEX) || true
	pdflatex $(FLAGS_LATEX) $(SOURCE_LATEX)

jabref: $(TARGET_JABREF)

$(TARGET_JABREF):
	DISPLAY= jabref $(FLAGS_JABREF) -i $(BASENAME).bib,bibtex -o $@,lifex

clean:
	$(RM) */*.aux */*.bbl */*.bcf */*.blg */*.log \
	      */*.out */*.run* */*.synctex* */*~

make distclean: clean
	$(RM) $(TARGET_LATEX) $(TARGET_JABREF)
