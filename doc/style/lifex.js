$( document ).ready(function() {
    // Show snow flakes only from 1st December to 6th January.
    var now = new Date();
    if (now.getMonth() == 11 || (now.getMonth() == 0 && now.getDate() <= 6)) {
        $('.snowflake').show();
    }
    else {
        $('.snowflake').hide();
    }
});
