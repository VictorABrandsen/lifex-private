## Changes between v1.5.0 and v1.6.0
- **New**: `MeshHandler` supports shared triangulations. (Federica Botta, Michele Bucelli, 2023/06/28).
- **New**: Implemented new classes for RBF interpolation between different meshes, and associated example. (Michele Bucelli, 2023/06/09).
- **New**: `BCHandler` and `MeshInfo` allow for more general mapping support. (Irena Radišić, 2023/04/01).
- **New**: Added the possibility of controlling the strategy that `NonLinearSolverHandler` uses to reset the linear solver initial guess for the nonlinear solver increment at each nonlinear solver iteration. (Francesco Regazzoni, 2023/03/11).
- **New**: added method `Laplace::apply_dirichlet_point`. (Michele Bucelli, 2023/02/27).
- **Improved**: Removed undesired behavior of `TimeInterpolation` in case a linear interpolation is required for a smaller value than the lower bound. Now the class has an option to either enable constant extrapolation or to raise an exception in case of extrapolation. (Francesco Regazzoni, 2023/02/23).
- **New**: added a command-line flag (`-t` or `--timer-statistics`). When enabled, at the end of execution, the minimum, maximum and average across parallel processes of the wall times are reported for each timer section. (Michele Bucelli, 2022/02/16).
- **Improved**: `MeshHandler` creates `parallel::fullydistributed::Triangulation`s in a more memory efficient way. (Michele Bucelli, 2023/01/17).
- **New**: `VTKFunction` allows scaling the geometry, not only the arrays. (Michele Bucelli, 2022/12/23).
- **Changed**: Updated param verbosity levels in linear and non-linear solvers. (Ivan Fumagalli, 2022/07/20).

- **Changed**: `Class`: changed y. (Name Surname, yyyy/mm/dd)
- **Improved**: `Class`: z now does a, b, c. (Name Surname, yyyy/mm/dd)
- **New**: `Class`: added x. (Name Surname, yyyy/mm/dd)

## [Previous versions](doc/changes)
- [Changes between v1.4.0 and v1.5.0](doc/changes/v1.4.0_vs_v1.5.0.md).
