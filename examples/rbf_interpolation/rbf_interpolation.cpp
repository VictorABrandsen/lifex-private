/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/core_model.hpp"
#include "source/init.hpp"

#include "source/geometry/mesh_handler.hpp"

#include "source/io/data_writer.hpp"

#include "source/numerics/projection.hpp"
#include "source/numerics/quaternions.hpp"
#include "source/numerics/rbf_interpolation.hpp"
#include "source/numerics/tools.hpp"

namespace lifex::examples
{
  /**
   * @brief Example class for RBF interpolation.
   *
   * The example considers two meshes (source and destination), geometrically
   * matching but with different discretizations, and evaluates the following
   * vector field on the source mesh:
   * @f[
   * \mathbf d(\mathbf x) = R(\mathbf x) \left( \mathbf b(\mathbf x) - \mathbf a
   * \right) + \mathbf a - \mathbf x\;,
   * @f]
   * where, setting @f$\mathbf x = \left(x_1, x_2, x_3\right)^T@f$,
   * - @f$R(\mathbf x)@f$ is a rotation matrix, with axis @f$(1, 0, 0)^T@f$ and
   * angle @f$\alpha = -\pi x_3@f$;
   * - @f$\mathbf a = \left(0, 0.05 + \pi^{-1}, 0\right)^T@f$;
   * - @f$\mathbf b(\mathbf x) = \left(x_1, x_2, 0\right)^T@f$.
   *
   * Then, the field is interpolated onto the destination mesh, in one of the
   * following ways:
   * 1. the vector field @f$\mathbf d@f$ is interpolated onto the vertices of
   * the destination mesh using DoF-to-DoF RBF interpolation (see
   * @ref utils::RBFInterpolationDoFs);
   * 2. the tensor field @f$F = I + \nabla\mathbf d@f$ is evaluated onto the
   * quadrature nodes of the destination mesh, interpolating its cartesian
   * components using quadrature-to-quadrature RBF interpolation (see
   * @ref utils::RBFInterpolationQuadrature);
   * 3. the tensor field @f$F = I + \nabla\mathbf d@f$ is evaluated onto the
   * quadrature nodes of the destination mesh, combining SVD factorization and
   * quadrature-to-quadrature RBF interpolation (see
   * @ref utils::RBFInterpolationQuadrature).
   *
   * **References:** @pubcite{bucelli2023preserving, Bucelli et al. (2023)}.
   */
  class ExampleRBFInterpolation : public CoreModel
  {
  public:
    /// Data to be interpolated.
    class InterpolatedData : public Function<dim>
    {
    public:
      /// Constructor.
      InterpolatedData()
        : Function<dim>(dim)
      {}

      /// Evaluation of the function.
      virtual void
      vector_value(const Point<dim> &p, Vector<double> &value) const override
      {
        const double     radius = 10 * L / M_PI;
        const Point<dim> origin = {0.0, radius + 0.5 * L, 0.0};
        Point<dim>       axis   = {1.0, 0.0, 0.0};
        axis                    = axis / axis.norm();

        const double theta_loc = -theta * p[2] / (10 * L);
        const double costheta  = std::cos(theta_loc);
        const double sintheta  = std::sin(theta_loc);

        Tensor<2, dim> R;

        R[0][0] = costheta + axis[0] * axis[0] * (1 - costheta);
        R[0][1] = axis[0] * axis[1] * (1 - costheta) - axis[2] * sintheta;
        R[0][2] = axis[0] * axis[2] * (1 - costheta) + axis[1] * sintheta;
        R[1][0] = axis[1] * axis[0] * (1 - costheta) + axis[2] * sintheta;
        R[1][1] = costheta + axis[1] * axis[1] * (1 - costheta);
        R[1][2] = axis[1] * axis[2] * (1 - costheta) - axis[0] * sintheta;
        R[2][0] = axis[2] * axis[0] * (1 - costheta) - axis[1] * sintheta;
        R[2][1] = axis[2] * axis[1] * (1 - costheta) + axis[0] * sintheta;
        R[2][2] = costheta + axis[2] * axis[2] * (1 - costheta);

        const Point<dim>     p0 = {p[0], p[1], 0};
        const Tensor<1, dim> x1 = R * (p0 - origin) + origin;

        for (unsigned int d = 0; d < value.size(); ++d)
          value[d] = x1[d] - p[d];
      }

      /// Evaluation of one component of the function.
      virtual double
      value(const Point<dim> &p, unsigned int component = 0) const override
      {
        Vector<double> vec_val(dim);
        vector_value(p, vec_val);
        return vec_val[component];
      }

    protected:
      /// Length of the side of the domain.
      const double L = 0.1;

      /// Torsion of the left face.
      const double theta = M_PI;
    };

    /// Quadrature evaluation for the deformation gradient. Returns a vector of
    /// dim * dim entries, corresponding to the components of the gradient in
    /// row-major order (i.e., (0, 0), (0, 1), (0, 2), (1, 0), (1, 1), ...)
    class QuadratureEvaluationF : public QuadratureEvaluationFEM<Vector<double>>
    {
    public:
      /// Constructor.
      QuadratureEvaluationF(const DoFHandler<dim> &    dof_handler,
                            const LinAlg::MPI::Vector &d_,
                            const Quadrature<dim> &    quadrature)
        : QuadratureEvaluationFEM<Vector<double>>(dof_handler,
                                                  quadrature,
                                                  update_gradients)
        , d(d_)
        , result(dim * dim)
      {}

      /// Call operator.
      virtual Vector<double>
      operator()(const unsigned int &q,
                 const double & /*t*/       = 0,
                 const Point<dim> & /*x_q*/ = Point<dim>()) override
      {
        F.clear();

        for (unsigned int i = 0; i < dof_indices.size(); ++i)
          {
            const unsigned int c =
              dof_handler->get_fe().system_to_component_index(i).first;
            const types::global_dof_index &dof_i = dof_indices[i];

            for (unsigned int j = 0; j < dim; ++j)
              F[c][j] += d[dof_i] * fe_values->shape_grad(i, q)[j];
          }

        F = utils::kinematics::F(F);

        for (unsigned int i = 0; i < dim; ++i)
          for (unsigned int j = 0; j < dim; ++j)
            result[i * dim + j] = F[i][j];

        return result;
      }

    protected:
      /// Reference to the displacement vector (including ghost entries).
      const LinAlg::MPI::Vector &d;

      /// Deformation gradient tensor (preallocated for speed).
      Tensor<2, dim> F;

      /// Vector containing the deformation gradient entries (preallocated for
      /// speed).
      Vector<double> result;
    };

    /// Class that evaluates the SVD decomposition of the deformation gradient.
    class QuadratureEvaluationSVDF
      : public QuadratureEvaluationFEM<Vector<double>>
    {
    public:
      /// Constructor.
      QuadratureEvaluationSVDF(const DoFHandler<dim> &    dof_handler,
                               const LinAlg::MPI::Vector &displacement_,
                               const Quadrature<dim> &    quadrature)
        : QuadratureEvaluationFEM<Vector<double>>(dof_handler,
                                                  quadrature,
                                                  update_gradients)
        , displacement(displacement_)
      {}

      /// Call operator.
      virtual Vector<double>
      operator()(const unsigned int &q,
                 const double & /*t*/       = 0,
                 const Point<dim> & /*x_q*/ = Point<dim>()) override
      {
        Vector<double> result(11);

        // We compute the local deformation gradient tensor.
        F = 0.0;
        for (unsigned int i = 0; i < dof_indices.size(); ++i)
          {
            const unsigned int component =
              dof_handler->get_fe().system_to_component_index(i).first;
            const unsigned int &dof_i = dof_indices[i];

            for (unsigned int d = 0; d < dim; ++d)
              F[component][d] +=
                displacement[dof_i] * fe_values->shape_grad(i, q)[d];
          }
        F = utils::kinematics::F(F);

        // Then, we compute its SVD decomposition.
        std::tie(U, S, VT) = utils::aligned_svd(F);

        // The U and VT matrices must be converted to quaternions.
        q_U  = utils::rotation_to_quaternion(U);
        q_VT = utils::rotation_to_quaternion(VT);

        // First four components of the result are the components of q_U.
        result[0] = q_U.a;
        result[1] = q_U.b;
        result[2] = q_U.c;
        result[3] = q_U.d;

        // Next three components are the singular values.
        result[4] = std::log(S[0]);
        result[5] = std::log(S[1]);
        result[6] = std::log(S[2]);

        // Last four components are the components of q_V.
        result[7]  = q_VT.a;
        result[8]  = q_VT.b;
        result[9]  = q_VT.c;
        result[10] = q_VT.d;

        return result;
      }

    protected:
      /// Reference to the displacement vector.
      const LinAlg::MPI::Vector &displacement;

      /// Deformation gradient (preallocated for speed).
      Tensor<2, dim> F;

      /// SVD factor of the deformation gradient (preallocated for speed).
      Tensor<2, dim> U;

      /// Quaternion representation of U (preallocated for speed).
      utils::Quaternion q_U;

      /// SVD factor of the deformation gradient (preallocated for speed).
      Tensor<2, dim> VT;

      /// Quaternion representation of VT (preallocated for speed).
      utils::Quaternion q_VT;

      /// Singular values of the deformation gradient (preallocated for speed).
      std::array<double, dim> S;
    };

    /// Concrete RBF interpolator for the element-wise interpolation of the
    /// deformation gradient. Returns the determinant of the interpolated
    /// deformation gradient.
    class RBFInterpolationElementWiseF
      : public utils::RBFInterpolationQuadrature<double>
    {
    public:
      /// Constructor.
      RBFInterpolationElementWiseF(const std::string &subsection)
        : RBFInterpolationQuadrature<double>(subsection)
      {}

      /// Setup system.
      void
      setup_system(const DoFHandler<dim> &dof_handler_src,
                   const Quadrature<dim> &quadrature_src,
                   const DoFHandler<dim> &dof_handler_dst,
                   const Quadrature<dim> &quadrature_dst)
      {
        // Call the setup method of the parent class, with dim * dim components.
        RBFInterpolationQuadrature<double>::setup_system(dim * dim,
                                                         dof_handler_src,
                                                         quadrature_src,
                                                         dof_handler_dst,
                                                         quadrature_dst);
      }

      /// Call operator.
      virtual double
      operator()(const unsigned int &q,
                 const double & /*t*/       = 0,
                 const Point<dim> & /*x_q*/ = Point<dim>()) override
      {
        Vector<double> F_vec(dim * dim);

        for (unsigned int i = 0; i < n_components; ++i)
          F_vec[i] = interpolated_vector[i][offset_dst + cell_offset + q];

        return F_vec[0] * F_vec[4] * F_vec[8] + F_vec[1] * F_vec[5] * F_vec[6] +
               F_vec[2] * F_vec[3] * F_vec[7] - F_vec[0] * F_vec[5] * F_vec[7] -
               F_vec[1] * F_vec[3] * F_vec[8] - F_vec[2] * F_vec[4] * F_vec[6];
      }

    protected:
    };

    /// Concrete RBF interpolator for the SVD interpolation of the deformation
    /// gradient.
    class RBFInterpolationSVDF
      : public utils::RBFInterpolationQuadrature<double>
    {
    public:
      /// Constructor.
      RBFInterpolationSVDF(const std::string &subsection)
        : RBFInterpolationQuadrature<double>(subsection)
      {}

      /// Setup system.
      void
      setup_system(const DoFHandler<dim> &dof_handler_src,
                   const Quadrature<dim> &quadrature_src,
                   const DoFHandler<dim> &dof_handler_dst,
                   const Quadrature<dim> &quadrature_dst)
      {
        // Call the setup method of the parent class, with 11 components.
        utils::RBFInterpolationQuadrature<double>::setup_system(
          11, dof_handler_src, quadrature_src, dof_handler_dst, quadrature_dst);

        F.resize(quadrature_dst.size());
        U.resize(quadrature_dst.size());
        VT.resize(quadrature_dst.size());
        S.resize(quadrature_dst.size());
      }

      /// Call operator. Return the determinant of the deformation gradient.
      virtual double
      operator()(const unsigned int &q,
                 const double & /*t*/       = 0,
                 const Point<dim> & /*x_q*/ = Point<dim>()) override
      {
        return S[q][0] * S[q][1] * S[q][2];
      }

    protected:
      /// Callback after reinit.
      void
      post_reinit_callback(
        const DoFHandler<dim>::active_cell_iterator &cell_other)
      {
        RBFInterpolationQuadrature<double>::post_reinit_callback(cell_other);

        for (unsigned int q = 0; q < fe_values->get_quadrature().size(); ++q)
          {
            const unsigned int idx = offset_dst + cell_offset + q;

            // Extract the quaternion for U from the interpolated data.
            q_U.a = interpolated_vector[0][idx];
            q_U.b = interpolated_vector[1][idx];
            q_U.c = interpolated_vector[2][idx];
            q_U.d = interpolated_vector[3][idx];

            // Recompute the matrix U.
            U[q] = utils::quaternion_to_rotation(q_U, true);

            // Extract the singular values from the interpolated data.
            S[q][0] = std::exp(interpolated_vector[4][idx]);
            S[q][1] = std::exp(interpolated_vector[5][idx]);
            S[q][2] = std::exp(interpolated_vector[6][idx]);

            // Extract the quaternion for V from the interpolated data.
            q_VT.a = interpolated_vector[7][idx];
            q_VT.b = interpolated_vector[8][idx];
            q_VT.c = interpolated_vector[9][idx];
            q_VT.d = interpolated_vector[10][idx];

            // Recompute the matrix VT.
            VT[q] = utils::quaternion_to_rotation(q_VT, true);
          }
      }

      /// Deformation gradient on quadrature nodes.
      std::vector<Tensor<2, dim>> F;

      /// SVD factor of the deformation gradient on quadrature nodes.
      std::vector<Tensor<2, dim>> U;

      /// SVD factor of the deformation gradient on quadrature nodes.
      std::vector<Tensor<2, dim>> VT;

      /// Singular values of the deformation gradient on quadrature nodes.
      std::vector<std::array<double, dim>> S;

      /// Quaternion representation of U (preallocated for speed).
      utils::Quaternion q_U;

      /// Quaternion representation of VT (preallocated for speed).
      utils::Quaternion q_VT;

      /// Inverse of the deformation gradient (preallocatd for speed).
      Tensor<2, dim> F_inv;
    };

    /// Constructor.
    ExampleRBFInterpolation(const std::string &subsection)
      : CoreModel(subsection)
      , mesh_src(prm_subsection_path + " / Source",
                 mpi_comm,
                 {utils::MeshHandler::GeometryType::File,
                  utils::MeshHandler::GeometryType::Hypercube})
      , mesh_dst(prm_subsection_path + " / Destination",
                 mpi_comm,
                 {utils::MeshHandler::GeometryType::File,
                  utils::MeshHandler::GeometryType::Hypercube})
      , rbf_interpolation_dofs(prm_subsection_path + " / RBF interpolation")
      , rbf_interpolation_element_wise(prm_subsection_path +
                                       " / RBF interpolation")
      , rbf_interpolation_svd(prm_subsection_path + " / RBF interpolation")
    {}

    /// Override of @ref CoreModel::declare_parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override
    {
      params.enter_subsection_path(prm_subsection_path);
      {
        params.declare_entry_selection(
          "Interpolation method",
          "DoFs displacement",
          "DoFs displacement|Element-wise gradient|SVD gradient",
          "\"DoFs displacement\" performs DoF-to-DoF interpolation of the "
          "displacement field. \"Element-wise gradient\" interpolates the "
          "deformation gradient element by element. \"SVD gradient\" "
          "interpolates the deformation gradient using SVD factorization. The "
          "last two methods perform quadrature-to-quadrature interpolation.");

        params.enter_subsection("Source");
        params.enter_subsection("Mesh and space discretization");
        params.declare_entry("FE space degree",
                             "1",
                             Patterns::Integer(1),
                             "Finite element degree used on the source mesh.");
        params.leave_subsection();
        params.leave_subsection();

        params.enter_subsection("Destination");
        params.enter_subsection("Mesh and space discretization");
        params.declare_entry(
          "FE space degree",
          "1",
          Patterns::Integer(1),
          "Finite element degree used on the destination mesh.");
        params.leave_subsection();
        params.leave_subsection();
      }
      params.leave_subsection_path();

      mesh_src.declare_parameters(params);
      mesh_dst.declare_parameters(params);

      rbf_interpolation_dofs.declare_parameters(params);
      rbf_interpolation_element_wise.declare_parameters(params);
      rbf_interpolation_svd.declare_parameters(params);
    }

    /// Override of @ref CoreModel::parse_parameters.
    virtual void
    parse_parameters(ParamHandler &params) override
    {
      params.parse();

      params.enter_subsection_path(prm_subsection_path);
      {
        prm_interpolation_method = params.get("Interpolation method");

        params.enter_subsection("Source");
        params.enter_subsection("Mesh and space discretization");
        prm_degree_src = params.get_integer("FE space degree");
        params.leave_subsection();
        params.leave_subsection();

        params.enter_subsection("Destination");
        params.enter_subsection("Mesh and space discretization");
        prm_degree_dst = params.get_integer("FE space degree");
        params.leave_subsection();
        params.leave_subsection();
      }
      params.leave_subsection_path();

      mesh_src.parse_parameters(params);
      mesh_dst.parse_parameters(params);

      rbf_interpolation_dofs.parse_parameters(params);
      rbf_interpolation_element_wise.parse_parameters(params);
      rbf_interpolation_svd.parse_parameters(params);
    }

    /// Run the example.
    virtual void
    run() override
    {
      // Create meshes.
      mesh_src.create_mesh();
      mesh_dst.create_mesh();

      // Initialize the DoF handlers.
      {
        const auto fe_src_scalar = mesh_src.get_fe_lagrange(prm_degree_src);
        fe_src = std::make_unique<FESystem<dim>>(*fe_src_scalar, dim);
        dof_handler_src.reinit(mesh_src.get());
        dof_handler_src.distribute_dofs(*fe_src);

        if (prm_interpolation_method == "DoFs displacement")
          {
            const auto fe_dst_scalar = mesh_dst.get_fe_lagrange(prm_degree_dst);
            fe_dst = std::make_unique<FESystem<dim>>(*fe_dst_scalar, dim);
          }
        else
          {
            // When interpolating the gradient, we need a scalar,
            // piecewise-constant FE space.
            fe_dst = mesh_dst.get_fe_dg(1);
          }

        dof_handler_dst.reinit(mesh_dst.get());
        dof_handler_dst.distribute_dofs(*fe_dst);
      }

      // Initialize vectors to store data on the two meshes.
      {
        IndexSet owned_dofs_src = dof_handler_src.locally_owned_dofs();
        IndexSet relevant_dofs_src;
        DoFTools::extract_locally_relevant_dofs(dof_handler_src,
                                                relevant_dofs_src);
        data_src_owned.reinit(owned_dofs_src, mpi_comm);
        data_src.reinit(owned_dofs_src, relevant_dofs_src, mpi_comm);

        IndexSet owned_dofs_dst = dof_handler_dst.locally_owned_dofs();
        IndexSet relevant_dofs_dst;
        DoFTools::extract_locally_relevant_dofs(dof_handler_dst,
                                                relevant_dofs_dst);
        data_dst_owned.reinit(owned_dofs_dst, mpi_comm);
        data_dst.reinit(owned_dofs_dst, relevant_dofs_dst, mpi_comm);
      }

      // Interpolate a function onto the fine mesh and write it to file.
      VectorTools::interpolate(dof_handler_src,
                               InterpolatedData(),
                               data_src_owned);
      data_src = data_src_owned;

      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path + " / Output");
        DataOut<dim>       data_out_src;
        data_out_src.add_data_vector(
          dof_handler_src,
          data_src,
          std::vector<std::string>(dim, "data_src"),
          std::vector<DataComponentInterpretation::DataComponentInterpretation>(
            dim, DataComponentInterpretation::component_is_part_of_vector));
        data_out_src.build_patches();
        utils::dataout_write_hdf5(data_out_src, "data_src");
        data_out_src.clear();
      }

      if (prm_interpolation_method == "DoFs displacement")
        run_dofs_displacement();
      else if (prm_interpolation_method == "Element-wise gradient")
        run_element_wise_gradient();
      else if (prm_interpolation_method == "SVD gradient")
        run_svd_gradient();

      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path + " / Output");
        DataOut<dim>       data_out_dst;

        if (prm_interpolation_method == "DoFs displacement")
          {
            data_out_dst.add_data_vector(
              dof_handler_dst,
              data_dst,
              std::vector<std::string>(dim, "data_dst"),
              std::vector<
                DataComponentInterpretation::DataComponentInterpretation>(
                dim, DataComponentInterpretation::component_is_part_of_vector));
          }
        else
          {
            data_out_dst.add_data_vector(dof_handler_dst, data_dst, "data_dst");
          }

        data_out_dst.build_patches();

        // If we interpolate the gradient, we don't filter duplicate vertices,
        // so that we can have piecewise constant quantities in output.
        utils::dataout_write_hdf5(
          data_out_dst,
          "data_dst",
          /* filter_duplicate_vertices = */ prm_interpolation_method ==
            "DoFs displacement");

        data_out_dst.clear();
      }
    }

  protected:
    /// Test the interpolation on DoFs.
    void
    run_dofs_displacement()
    {
      // Setup the RBF interpolant.
      rbf_interpolation_dofs.setup_system(dof_handler_src, dof_handler_dst);

      pcout << utils::log::separator_section << std::endl;

      // Compute the interpolation from the fine to the coarse mesh
      // and write it to file.
      pcout << "RBF interpolation:" << std::flush;
      rbf_interpolation_dofs.interpolate(data_dst_owned, data_src_owned);
      data_dst = data_dst_owned;
    }

    /// Test the element-wise interpolation of the deformation gradient.
    void
    run_element_wise_gradient()
    {
      const auto quadrature_src = mesh_src.get_quadrature_gauss(1);
      const auto quadrature_dst = mesh_dst.get_quadrature_gauss(1);

      // Setup the RBF interpolant.
      rbf_interpolation_element_wise.setup_system(dof_handler_src,
                                                  *quadrature_src,
                                                  dof_handler_dst,
                                                  *quadrature_dst);

      pcout << utils::log::separator_section << std::endl;

      // Build a quadrature evaluator to evaluate the interpolated quantity (the
      // deformation gradient) on the quadrature nodes of source mesh.
      QuadratureEvaluationF quadrature_evaluation_src(dof_handler_src,
                                                      data_src,
                                                      *quadrature_src);

      // Compute the interpolation write it to file.
      pcout << "RBF interpolation of F (using element-wise interpolation):"
            << std::endl;

      rbf_interpolation_element_wise.interpolate(quadrature_evaluation_src);

      {
        std::vector<types::global_dof_index> dof_indices(fe_dst->dofs_per_cell);

        rbf_interpolation_element_wise.init();
        for (const auto &cell : dof_handler_dst.active_cell_iterators())
          {
            if (!cell->is_locally_owned())
              continue;

            rbf_interpolation_element_wise.reinit(cell);
            cell->get_dof_indices(dof_indices);

            for (const auto &dof : dof_indices)
              utils::set_vector_entry(data_dst_owned,
                                      dof,
                                      rbf_interpolation_element_wise(0));
          }

        data_dst_owned.compress(VectorOperation::insert);
        data_dst = data_dst_owned;
      }
    }

    /// Test the SVD-based interpolation of the deformation gradient.
    void
    run_svd_gradient()
    {
      const auto quadrature_src = mesh_src.get_quadrature_gauss(1);
      const auto quadrature_dst = mesh_dst.get_quadrature_gauss(1);

      // Setup the RBF interpolant.
      rbf_interpolation_svd.setup_system(dof_handler_src,
                                         *quadrature_src,
                                         dof_handler_dst,
                                         *quadrature_dst);

      pcout << utils::log::separator_section << std::endl;

      // Build a quadrature evaluator to evaluate the interpolated quantity (the
      // deformation gradient) on the quadrature nodes of source mesh.
      QuadratureEvaluationSVDF quadrature_evaluation_src(dof_handler_src,
                                                         data_src,
                                                         *quadrature_src);

      // Compute the interpolation write it to file.
      pcout << "RBF interpolation of F (using SVD):" << std::endl;

      rbf_interpolation_svd.interpolate(quadrature_evaluation_src);

      {
        std::vector<types::global_dof_index> dof_indices(fe_dst->dofs_per_cell);

        rbf_interpolation_svd.init();
        for (const auto &cell : dof_handler_dst.active_cell_iterators())
          {
            if (!cell->is_locally_owned())
              continue;

            rbf_interpolation_svd.reinit(cell);
            cell->get_dof_indices(dof_indices);

            for (const auto &dof : dof_indices)
              utils::set_vector_entry(data_dst_owned,
                                      dof,
                                      rbf_interpolation_svd(0));
          }

        data_dst_owned.compress(VectorOperation::insert);
        data_dst = data_dst_owned;
      }
    }

    /// Source mesh.
    utils::MeshHandler mesh_src;

    /// Source DoF handler.
    DoFHandler<dim> dof_handler_src;

    /// Finite element space on the source mesh.
    std::unique_ptr<FiniteElement<dim>> fe_src;

    /// Data vector on the source mesh, without ghost elements.
    LinAlg::MPI::Vector data_src_owned;

    /// Data vector on the source mesh, including ghost elements.
    LinAlg::MPI::Vector data_src;

    /// Destination mesh.
    utils::MeshHandler mesh_dst;

    /// Destination DoF handler.
    DoFHandler<dim> dof_handler_dst;

    /// Finite element space on the destination mesh.
    std::unique_ptr<FiniteElement<dim>> fe_dst;

    /// Data vector on the destination mesh, without ghost elements.
    LinAlg::MPI::Vector data_dst_owned;

    /// Data vector on the source mesh, including ghost elements.
    LinAlg::MPI::Vector data_dst;

    /// RBF interpolation on DoFs.
    utils::RBFInterpolationDoFs rbf_interpolation_dofs;

    /// RBF interpolation of F, element-wise.
    RBFInterpolationElementWiseF rbf_interpolation_element_wise;

    /// RBF interpolation of F using SVD.
    RBFInterpolationSVDF rbf_interpolation_svd;

    /// @name Parameters read from file.
    /// @{

    /// Interpolation points (DoFs or quadrature points).
    std::string prm_interpolation_method;

    /// Finite element degree used on the source mesh.
    unsigned int prm_degree_src;

    /// Finite element degree used on the destination mesh.
    unsigned int prm_degree_dst;

    /// @}
  }; // namespace lifex::examples
} // namespace lifex::examples


/// Example RBF interpolation.
int
main(int argc, char **argv)
{
  lifex::lifex_init lifex_initializer(argc, argv, 1);

  try
    {
      lifex::examples::ExampleRBFInterpolation example(
        "Example RBF interpolation");
      example.main_run_generate();
    }
  LIFEX_CATCH_EXC();

  return EXIT_SUCCESS;
}
