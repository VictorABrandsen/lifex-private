/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/geometry/mesh_handler.hpp"

#include "source/numerics/numbers.hpp"
#include "source/numerics/rbf_interpolation.hpp"
#include "source/numerics/tools.hpp"

#include <algorithm>
#include <deque>
#include <fstream>
#include <limits>
#include <vector>

namespace lifex::utils
{
  RBFInterpolation::RBFInterpolation(const std::string &subsection)
    : CoreModel(subsection)
    , linear_solver(subsection + " / Linear solver", {"CG", "GMRES"}, "CG")
    , preconditioner_handler(subsection + " / Preconditioner")
  {}

  void
  RBFInterpolation::declare_parameters(ParamHandler &params) const
  {
    params.enter_subsection_path(prm_subsection_path);

    params.declare_entry("Adaptive RBF support",
                         "true",
                         Patterns::Bool(),
                         "If true, the RBF support radius is adaptively "
                         "selected for every point in the source mesh.");

    params.declare_entry("Adaptive RBF neighbors",
                         "10",
                         Patterns::Integer(1),
                         "Number of nearest neighbors included in the support "
                         "of each RBF, if adaptive support is enabled.");

    params.declare_entry("Support increase factor",
                         "2.0",
                         Patterns::Double(1.0),
                         "If adaptive RBF support is enabled, the support for "
                         "a given source point is the distance from its N-th "
                         "nearest neighbor upscaled by this factor.");

    params.declare_entry(
      "RBF support radius",
      "0.1",
      Patterns::Double(0),
      "Fixed support radius of the RBFs used in the interpolation (ignored if "
      "adaptive support is enabled).");

    params.declare_entry("Rescale",
                         "true",
                         Patterns::Bool(),
                         "If true, the interpolant is rescaled by the "
                         "interpolant of the constant function equal to 1.");

    params.declare_entry(
      "Material ID groups",
      "",
      Patterns::List(Patterns::List(Patterns::Integer(0),
                                    0,
                                    Patterns::List::max_int_value,
                                    " "),
                     0),
      "A comma-separated list of space-separated lists (e.g. 0 1, 2 3 4) of "
      "material IDs. Points will be labelled according to the list their "
      "material ID appears in (in previous example, material IDs 0 and 1 are "
      "labelled as 0, while IDs 2, 3 and 4 are labelled as 1). Interpolation "
      "only happens between points with the same label. Leave empty for no "
      "labeling.");

    params.enter_subsection("Serialization");
    {
      params.declare_entry("Serialize matrices to file",
                           "true",
                           Patterns::Bool(),
                           "If true, the interpolation and evaluation matrices "
                           "are serialized to file.");

      params.declare_entry(
        "Serialization filename",
        "rbf-interpolation",
        Patterns::FileName(Patterns::FileName::FileType::output),
        "Name of the file where the matrices will be serialized. The path is "
        "relative to the output directory.");

      params.declare_entry(
        "Deserialize matrices from file",
        "false",
        Patterns::Bool(),
        "If true, the interpolation and evaluation matrices "
        "are deserialized from a file created by a previous run.");

      params.declare_entry(
        "Deserialization filename",
        "",
        Patterns::FileName(Patterns::FileName::FileType::input),
        "Name of the file from which the matrices will be deserialized. The "
        "path is either absolute or relative to the executable path.");
    }
    params.leave_subsection();

    params.leave_subsection_path();

    linear_solver.declare_parameters(params);

    params.enter_subsection_path(
      prm_subsection_path + " / Preconditioner / Approximate cardinal basis");
    {
      params.declare_entry("Local solver reduction",
                           "1e-2",
                           Patterns::Double(0),
                           "Relative tolerance of the local solver used to "
                           "assemble the preconditioner.");
    }
    params.leave_subsection_path();

    preconditioner_handler.declare_parameters(params);

    // Override preconditioner types to add the approximate cardinal basis.
    params.enter_subsection_path(prm_subsection_path + " / Preconditioner");
    params.declare_entry_selection("Preconditioner type",
                                   "Approximate cardinal basis",
                                   "Approximate cardinal basis | " +
                                     PreconditionerHandler::types,
                                   "Determine which preconditioner to use");
    params.leave_subsection_path();
  }

  void
  RBFInterpolation::parse_parameters(ParamHandler &params)
  {
    params.parse();

    params.enter_subsection_path(prm_subsection_path);

    prm_adaptive_rbf_support    = params.get_bool("Adaptive RBF support");
    prm_adaptive_neighbors      = params.get_integer("Adaptive RBF neighbors");
    prm_support_increase_factor = params.get_double("Support increase factor");
    prm_rbf_support             = params.get_double("RBF support radius");
    prm_rescale                 = params.get_bool("Rescale");

    const std::vector<std::string> groups =
      params.get_vector<std::string>("Material ID groups");
    for (unsigned int label = 0; label < groups.size(); ++label)
      {
        const std::vector<unsigned int> ids =
          utils::string_to_vector<unsigned int>(groups[label], {}, true, " ");

        // We shift the labels by 1 (label 0 is reserved for unlabeled points).
        for (const auto &id : ids)
          prm_map_id_label[id] = (label + 1);
      }

    params.enter_subsection("Serialization");
    {
      prm_serialize_matrices = params.get_bool("Serialize matrices to file");
      prm_serialization_filename = params.get("Serialization filename");
      prm_deserialize_matrices =
        params.get_bool("Deserialize matrices from file");
      prm_deserialization_filename = params.get("Deserialization filename");
    }
    params.leave_subsection();

    params.leave_subsection_path();

    linear_solver.parse_parameters(params);

    params.enter_subsection_path(prm_subsection_path + " / Preconditioner");
    {
      prm_preconditioner_type = params.get("Preconditioner type");

      if (prm_preconditioner_type == "Approximate cardinal basis")
        {
          params.enter_subsection("Approximate cardinal basis");
          prm_prec_reduction = params.get_double("Local solver reduction");
          params.leave_subsection();
        }
    }
    params.leave_subsection_path();

    if (prm_preconditioner_type != "Approximate cardinal basis")
      preconditioner_handler.parse_parameters(params);
  }

  void
  RBFInterpolation::serialize_matrices(const std::string &filename) const
  {
    auto serialize_matrix = [](const std::string &              filename,
                               const LinAlg::MPI::SparseMatrix &matrix,
                               const unsigned int &             local_nnz) {
      std::vector<unsigned int> rows(local_nnz);
      std::vector<unsigned int> cols(local_nnz);
      std::vector<double>       vals(local_nnz);
      unsigned int              n_entry = 0;

// PETSc only supports iterators on rows, for distributed matrices. However,
// iterating directly on matrix entries is much faster on Trilinos.
#if defined(LIN_ALG_TRILINOS)
      for (const auto &entry : matrix)
        {
          rows[n_entry] = entry.row();
          cols[n_entry] = entry.column();
          vals[n_entry] = entry.value();
          ++n_entry;
        }
#elif defined(LIN_ALG_PETSC)
      for (const auto &row : matrix.locally_owned_range_indices())
        for (auto entry = matrix.begin(row); entry != matrix.end(row); ++entry)
          {
            rows[n_entry] = entry->row();
            cols[n_entry] = entry->column();
            vals[n_entry] = entry->value();
            ++n_entry;
          }
#endif

      // Each process appends its local entries to the output file, one at a
      // time.
      for (unsigned int rank = 0; rank < Core::mpi_size; ++rank)
        {
          if (Core::mpi_rank == rank)
            {
              std::ofstream file(prm_output_directory + filename,
                                 std::ios::out | std::ios_base::app |
                                   std::ios::binary);

              file.write(reinterpret_cast<const char *>(&local_nnz),
                         sizeof(local_nnz));
              file.write(reinterpret_cast<const char *>(rows.data()),
                         rows.size() * sizeof(unsigned int));
              file.write(reinterpret_cast<const char *>(cols.data()),
                         cols.size() * sizeof(unsigned int));
              file.write(reinterpret_cast<const char *>(vals.data()),
                         vals.size() * sizeof(double));
            }

          MPI_Barrier(Core::mpi_comm);
        }
    };

    // We write the number of MPI processes to the file, to check that
    // serialization and deserialization occur with the same number of cores,
    // so that we can output a meaningful error message if this doesn't happen.
    // This also creates and clears the file.
    if (mpi_rank == 0)
      {
        std::ofstream output_file(prm_output_directory + filename,
                                  std::ios::out | std::ios::binary);
        output_file.write(reinterpret_cast<const char *>(&mpi_size),
                          sizeof(mpi_size));
      }

    // All processes should wait for rank 0 to create and clear the output file,
    // before they start writing into it.
    MPI_Barrier(mpi_comm);

    serialize_matrix(filename, interpolation_matrix, nnz_interpolation);
    serialize_matrix(filename, evaluation_matrix, nnz_evaluation);

    if (prm_preconditioner_type == "Approximate cardinal basis")
      serialize_matrix(filename, preconditioner_matrix, nnz_preconditioner);
  }

  void
  RBFInterpolation::deserialize_matrices(const std::string &filename)
  {
    auto deserialize_matrix = [](std::ifstream &            stream,
                                 LinAlg::MPI::SparseMatrix &matrix,
                                 const IndexSet &           owned_rows,
                                 const IndexSet &           owned_cols) {
      std::vector<unsigned int> rows;
      std::vector<unsigned int> cols;
      std::vector<double>       vals;
      unsigned int              nnz = 0;

      for (unsigned int rank = 0; rank < Core::mpi_size; ++rank)
        {
          unsigned int tmp_nnz = 0;
          stream.read(reinterpret_cast<char *>(&tmp_nnz), sizeof(tmp_nnz));

          // If current block is owned by current rank, we read it.
          if (rank == Core::mpi_rank)
            {
              nnz = tmp_nnz;
              rows.resize(nnz);
              cols.resize(nnz);
              vals.resize(nnz);

              stream.read(reinterpret_cast<char *>(rows.data()),
                          rows.size() * sizeof(unsigned int));
              stream.read(reinterpret_cast<char *>(cols.data()),
                          cols.size() * sizeof(unsigned int));
              stream.read(reinterpret_cast<char *>(vals.data()),
                          vals.size() * sizeof(double));
            }
          // Otherwise, we move ahead in the file by nnz entries.
          else
            {
              stream.seekg(tmp_nnz *
                             (2 * sizeof(unsigned int) + sizeof(double)),
                           stream.cur);
            }
        }

      // We build the sparsity pattern, then initialize the matrix and fill it.
      DynamicSparsityPattern dsp(owned_rows.size(),
                                 owned_cols.size(),
                                 owned_rows);

      for (unsigned int i = 0; i < nnz; ++i)
        dsp.add(rows[i], cols[i]);

      SparsityTools::distribute_sparsity_pattern(dsp,
                                                 owned_rows,
                                                 Core::mpi_comm,
                                                 owned_rows);
      matrix.reinit(owned_rows, owned_cols, dsp, Core::mpi_comm);

      for (unsigned int i = 0; i < nnz; ++i)
        matrix.set(rows[i], cols[i], vals[i]);

      matrix.compress(VectorOperation::insert);
    };

    std::ifstream input_file(filename, std::ios::in | std::ios::binary);
    AssertThrow(input_file.is_open(), ExcFileNotOpen(filename));

    unsigned int serialization_mpi_size = 0;
    input_file.read(reinterpret_cast<char *>(&serialization_mpi_size),
                    sizeof(serialization_mpi_size));

    AssertThrow(mpi_size == serialization_mpi_size,
                ExcMessage(
                  "Deserialization of interpolation matrices must be done with "
                  " the same number of processes as serialization "
                  "(serialization processes: " +
                  std::to_string(serialization_mpi_size) +
                  ", deserialization processes: " + std::to_string(mpi_size) +
                  "."));

    deserialize_matrix(input_file,
                       interpolation_matrix,
                       owned_points_src,
                       owned_points_src);
    deserialize_matrix(input_file,
                       evaluation_matrix,
                       owned_points_dst,
                       owned_points_src);

    if (prm_preconditioner_type == "Approximate cardinal basis")
      deserialize_matrix(input_file,
                         preconditioner_matrix,
                         owned_points_src,
                         owned_points_src);
  }

  void
  RBFInterpolation::setup_system_internal(
    const std::vector<PointMap> &points_src,
    const std::vector<PointMap> &points_dst)
  {
    Assert(points_src.size() == points_dst.size(),
           ExcDimensionMismatch(points_src.size(), points_dst.size()));

    // We collect interpolation points across all processes, because the
    // current rank needs to access points that it does not own but that are
    // closer than the RBF support to an owned point.
    std::vector<PointMap> all_points_src(points_src.size());

    // We convert maps to vectors, since iterating through them is more
    // cache-friendly and makes the setup significantly more efficient.
    std::vector<std::vector<PointMap::value_type>> points_src_vec(
      points_src.size());
    std::vector<std::vector<PointMap::value_type>> points_dst_vec(
      points_dst.size());
    std::vector<std::vector<PointMap::value_type>> all_points_src_vec(
      points_src.size());

    unsigned int n_points_src = 0;
    unsigned int n_points_dst = 0;

    for (unsigned int c = 0; c < points_src.size(); ++c)
      {
        points_src_vec[c] =
          std::vector<PointMap::value_type>(points_src[c].begin(),
                                            points_src[c].end());
        points_dst_vec[c] =
          std::vector<PointMap::value_type>(points_dst[c].begin(),
                                            points_dst[c].end());

        all_points_src[c] = MPI::compute_map_union(points_src[c], mpi_comm);
        all_points_src_vec[c] =
          std::vector<PointMap::value_type>(all_points_src[c].begin(),
                                            all_points_src[c].end());

        n_points_src += all_points_src[c].size();
        n_points_dst += Utilities::MPI::sum(points_dst[c].size(), mpi_comm);
      }

    pcout << utils::log::separator_section << std::endl;
    pcout << prm_subsection_path << std::endl;
    pcout << "    Source points:      " << n_points_src << std::endl
          << "    Destination points: " << n_points_dst << std::endl;

    if (!prm_map_id_label.empty())
      {
        pcout << utils::log::separator_subsection << std::endl;
        pcout << "    Grouping by material ID:" << std::endl;

        std::map<unsigned int, std::set<types::material_id>> map_label_ids;
        for (const auto &i : prm_map_id_label)
          map_label_ids[i.second].insert(i.first);

        for (const auto &set : map_label_ids)
          {
            std::string set_str = "";
            for (const auto &id : set.second)
              set_str += " " + std::to_string(id);

            pcout << "        group " << set.first << ":" << set_str
                  << std::endl;
          }
      }

    // Compute sets of locally owned points.
    owned_points_src.set_size(n_points_src);
    owned_points_dst.set_size(n_points_dst);
    for (unsigned int c = 0; c < points_src.size(); ++c)
      {
        for (const auto &i : points_src[c])
          owned_points_src.add_index(i.first);

        for (const auto &i : points_dst[c])
          owned_points_dst.add_index(i.first);
      }

    if (!prm_deserialize_matrices)
      {
        DynamicSparsityPattern dsp_interpolation(n_points_src,
                                                 n_points_src,
                                                 owned_points_src);
        DynamicSparsityPattern dsp_evaluation(n_points_dst,
                                              n_points_src,
                                              owned_points_dst);

        if (prm_adaptive_rbf_support)
          {
            TimerOutput::Scope timer_section(
              timer_output,
              prm_subsection_path +
                " / Setup system / Compute adaptive RBF support");

            pcout << utils::log::separator_subsection << std::endl;
            pcout << "    Computing adaptive RBF support radius" << std::endl;

            pcout << "        Nearest neighbors: " << prm_adaptive_neighbors
                  << std::endl;

            std::map<types::global_dof_index, double> rbf_supports_owned;

            double support_min = std::numeric_limits<double>::max();
            double support_max = 0.0;
            double support_avg = 0.0;

            for (unsigned int c = 0; c < points_src.size(); ++c)
              for (const auto &p : points_src_vec[c])
                {
                  const std::vector<std::pair<types::global_dof_index, double>>
                    nearest = find_closest_points(all_points_src_vec[c],
                                                  p,
                                                  prm_adaptive_neighbors);

                  // The distance of the n-th nearest neighbor is the RBF
                  // support radius for current DoF.
                  const double support = prm_support_increase_factor *
                                         std::sqrt(nearest.back().second);
                  rbf_supports_owned[p.first] = support;

                  // Compute statistics for logging.
                  support_min = std::min(support_min, support);
                  support_max = std::max(support_max, support);
                  support_avg += support;
                }

            rbf_supports = MPI::compute_map_union(rbf_supports_owned, mpi_comm);

            support_min = Utilities::MPI::min(support_min, mpi_comm);
            support_max = Utilities::MPI::max(support_max, mpi_comm);
            support_avg =
              Utilities::MPI::sum(support_avg, mpi_comm) / n_points_src;

            pcout << "        Minimum support: " << support_min << std::endl
                  << "        Average support: " << support_avg << std::endl
                  << "        Maximum support: " << support_max << std::endl;
          }

        // For each local point, we store in a map the indices of points that
        // are within range. This duplicates the information stored in the
        // sparsity of the interpolation matrix, but in a more efficiently
        // accessible way.
        std::map<types::global_dof_index, std::vector<types::global_dof_index>>
          neighbors_src;
        std::map<types::global_dof_index, std::vector<types::global_dof_index>>
          neighbors_dst;

        std::map<types::global_dof_index, unsigned int> component_src;
        std::map<types::global_dof_index, unsigned int> component_dst;

        // Setup the sparsity patterns and initialize the matrices.
        {
          TimerOutput::Scope timer_section(
            timer_output,
            prm_subsection_path + " / Setup system / Build sparsity");

          pcout << utils::log::separator_subsection << std::endl;
          pcout << "    Assembling interpolation matrices" << std::endl;

          nnz_interpolation = 0;
          nnz_evaluation    = 0;

          for (unsigned int c = 0; c < points_src.size(); ++c)
            {
              for (const auto &[i, p_i] : points_src_vec[c])
                component_src.insert({i, c});

              for (const auto &[i, p_i] : points_dst_vec[c])
                component_dst.insert({i, c});
            }

          for (unsigned int c = 0; c < points_src.size(); ++c)
            for (const auto &[j, p_j] : all_points_src_vec[c])
              {
                const double support_square = get_support_square(j);

                for (const auto &[i, p_i] : points_src_vec[c])
                  if (p_i.distance_square(p_j) <= support_square &&
                      p_i.shares_label_with(p_j))
                    {
                      dsp_interpolation.add(i, j);
                      neighbors_src[i].push_back(j);
                      ++nnz_interpolation;
                    }

                for (const auto &[i, p_i] : points_dst_vec[c])
                  if (p_i.distance_square(p_j) <= support_square &&
                      p_i.shares_label_with(p_j))
                    {
                      dsp_evaluation.add(i, j);
                      neighbors_dst[i].push_back(j);
                      ++nnz_evaluation;
                    }
              }

          SparsityTools::distribute_sparsity_pattern(dsp_interpolation,
                                                     owned_points_src,
                                                     mpi_comm,
                                                     owned_points_src);
          SparsityTools::distribute_sparsity_pattern(dsp_evaluation,
                                                     owned_points_dst,
                                                     mpi_comm,
                                                     owned_points_dst);

          const unsigned int n_nonzero_rows =
            Utilities::MPI::sum(dsp_evaluation.nonempty_rows().n_elements(),
                                mpi_comm);
          AssertThrow(n_nonzero_rows == n_points_dst,
                      ExcMessage(std::to_string(n_points_dst - n_nonzero_rows) +
                                 " points in the destination mesh are not "
                                 "reached by any RBF in the source mesh. "
                                 "Try increasing the RBF support radius."));

          interpolation_matrix.reinit(owned_points_src,
                                      owned_points_src,
                                      dsp_interpolation,
                                      mpi_comm);
          evaluation_matrix.reinit(owned_points_dst,
                                   owned_points_src,
                                   dsp_evaluation,
                                   mpi_comm);

          // Explicit conversion to double to avoid error due to overflow (when
          // multiplying the number of points) and integer division.
          const double n_points_src_double = static_cast<double>(n_points_src);
          pcout << "        Interpolation fill-in: "
                << interpolation_matrix.n_nonzero_elements() /
                     (n_points_src_double * n_points_src_double)
                << std::endl;
          pcout << "        Evaluation fill-in:    "
                << evaluation_matrix.n_nonzero_elements() /
                     (n_points_src_double * n_points_dst)
                << std::endl;
        }

        // Assemble the matrices, one component at a time.
        {
          TimerOutput::Scope timer_section(timer_output,
                                           prm_subsection_path +
                                             " / Setup system / Assembly");

          for (const auto &i : dsp_interpolation.row_index_set())
            {
              const unsigned int &c           = component_src.at(i);
              const auto &        point_i     = points_src[c].at(i);
              const auto &        neighbors_i = neighbors_src.at(i);

              for (const unsigned int &j : neighbors_i)
                {
                  const Point<dim> &point_j = all_points_src[c].at(j);

                  interpolation_matrix.set(
                    i, j, rbf(point_i.distance(point_j), get_support(j)));
                }
            }

          for (const auto &i : dsp_evaluation.row_index_set())
            {
              const unsigned int &c           = component_dst.at(i);
              const auto &        point_i     = points_dst[c].at(i);
              const auto &        neighbors_i = neighbors_dst.at(i);

              for (const unsigned int &j : neighbors_i)
                {
                  const Point<dim> &point_j = all_points_src[c].at(j);

                  evaluation_matrix.set(
                    i, j, rbf(point_i.distance(point_j), get_support(j)));
                }
            }

          interpolation_matrix.compress(VectorOperation::insert);
          evaluation_matrix.compress(VectorOperation::insert);
        }

        // Build the preconditioner.
        if (prm_preconditioner_type == "Approximate cardinal basis")
          {
            // Preconditioner for the local interpolation problems, applying the
            // Gauss-Seidel method.
            class LocalGaussSeidel
            {
            public:
              void
              vmult(Vector<double> &dst, const Vector<double> &src) const
              {
                for (unsigned int i = 0; i < matrix->n(); ++i)
                  {
                    dst(i) = src(i);

                    for (unsigned int j = 0; j < i; ++j)
                      dst(i) -= (*matrix)(i, j) * dst(j);
                  }
              }

              void
              initialize(const FullMatrix<double> &matrix)
              {
                this->matrix = &matrix;
              }

            protected:
              // Local matrix.
              const FullMatrix<double> *matrix;
            };

            pcout << utils::log::separator_subsection << std::endl;
            pcout << "    Initializing preconditioner" << std::endl;

            TimerOutput::Scope timer_section(
              timer_output,
              prm_subsection_path +
                " / Setup system / Preconditioner assembly");

            preconditioner_matrix.reinit(interpolation_matrix);
            preconditioner_matrix.copy_from(interpolation_matrix);

            nnz_preconditioner = nnz_interpolation;

            LocalGaussSeidel local_prec;

            for (const auto &row :
                 preconditioner_matrix.locally_owned_range_indices())
              {
                const unsigned int &c                = component_src.at(row);
                const auto &        neighbors_row    = neighbors_src.at(row);
                const auto &        all_points_src_c = all_points_src[c];

                const unsigned int n = neighbors_row.size();

                // Retrieve the coordinates of the neighbors for current point.
                std::vector<LabelledPoint> neighbors_points(n);
                for (unsigned int i = 0; i < n; ++i)
                  neighbors_points[i] = all_points_src_c.at(neighbors_row[i]);

                // Assemble the local interpolation problem.
                FullMatrix<double> matrix(n, n);
                Vector<double>     rhs(n);
                Vector<double>     sol(n);

                for (unsigned int j = 0; j < n; ++j)
                  {
                    const auto & p_j     = neighbors_points[j];
                    const double support = get_support(neighbors_row[j]);

                    for (unsigned int i = 0; i < n; ++i)
                      {
                        const auto & p_i             = neighbors_points[i];
                        const double distance_square = p_i.distance_square(p_j);

                        if (distance_square < support * support)
                          matrix(i, j) =
                            rbf(std::sqrt(distance_square), support);
                      }

                    if (neighbors_row[j] == row)
                      rhs[j] = 1.0;
                  }

                // Solve the local interpolation problem.
                ReductionControl reduction_control(1000, 0, prm_prec_reduction);
                SolverGMRES<Vector<double>> solver(reduction_control);

                local_prec.initialize(matrix);
                solver.solve(matrix, sol, rhs, local_prec);

                // Copy the results into the preconditioner.
                for (unsigned int i = 0; i < sol.size(); ++i)
                  preconditioner_matrix.set(row, neighbors_row[i], sol[i]);
              }

            preconditioner_matrix.compress(VectorOperation::insert);
          }

        if (prm_serialize_matrices)
          {
            pcout << utils::log::separator_subsection << std::endl;
            pcout << "    Serializing matrices to "
                  << prm_serialization_filename << std::endl;
            serialize_matrices(prm_serialization_filename);
          }
      }
    else
      {
        pcout << utils::log::separator_subsection << std::endl;
        pcout << "    Deserializing matrices from "
              << prm_deserialization_filename << std::endl;
        deserialize_matrices(prm_deserialization_filename);
      }

    if (prm_preconditioner_type != "Approximate cardinal basis")
      preconditioner_handler.initialize(interpolation_matrix);

    interpolation_coefficients_owned.reinit(owned_points_src, mpi_comm);

    if (prm_rescale)
      {
        pcout << utils::log::separator_subsection << std::endl;
        pcout << "    Interpolating constant function" << std::flush;

        interpolant_of_one_owned.reinit(owned_points_dst, mpi_comm);

        LinAlg::MPI::Vector vector_of_one(owned_points_src, mpi_comm);
        vector_of_one = 1.0;

        interpolate_internal(interpolant_of_one_owned, vector_of_one, true);
      }
  }

  void
  RBFInterpolation::interpolate_internal(LinAlg::MPI::Vector &      dst,
                                         const LinAlg::MPI::Vector &src,
                                         const bool &disable_rescaling)
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Interpolate");

    // Compute the interpolation coefficients by inverting the interpolation
    // matrix.
    if (prm_preconditioner_type == "Approximate cardinal basis")
      linear_solver.solve(interpolation_matrix,
                          interpolation_coefficients_owned,
                          src,
                          utils::Transpose(preconditioner_matrix));
    else
      linear_solver.solve(interpolation_matrix,
                          interpolation_coefficients_owned,
                          src,
                          preconditioner_handler);

    // Compute the interpolant.
    evaluation_matrix.vmult(dst, interpolation_coefficients_owned);

    // Apply rescaling if needed.
    if (prm_rescale && !disable_rescaling)
      for (const auto &i : dst.locally_owned_elements())
        dst[i] /= interpolant_of_one_owned[i];

    dst.compress(VectorOperation::insert);
  }

  double
  RBFInterpolation::rbf(const double &r, const double &support) const
  {
    const double r_rel = r / support;
    const double a     = 1.0 - r_rel;

    if (a > 0)
      return a * a * a * a * (1.0 + 4.0 * r_rel);
    else
      return 0.0;
  }

  std::vector<std::pair<types::global_dof_index, double>>
  RBFInterpolation::find_closest_points(
    const std::vector<PointMap::value_type> &points,
    const PointMap::value_type &             origin,
    const unsigned int &                     N)
  {
    if (N == 1)
      {
        std::pair<types::global_dof_index, double> nearest =
          std::make_pair(0, std::numeric_limits<double>::infinity());

        for (const auto &[j, p_j] : points)
          {
            if (j == origin.first)
              continue;

            const double d2 = p_j.distance_square(origin.second);

            if (d2 < nearest.second)
              {
                nearest.first  = j;
                nearest.second = d2;
              }
          }

        return std::vector<std::pair<types::global_dof_index, double>>{nearest};
      }
    else
      {
        // We build a vector that stores the indices and squared distances of
        // the N nearest neighbors, in order of ascending distance.
        std::vector<std::pair<types::global_dof_index, double>> nearest;

        auto is_closer =
          [](const std::pair<types::global_dof_index, double> &a,
             const std::pair<types::global_dof_index, double> &b) {
            return a.second < b.second;
          };

        for (const auto &[j, p_j] : points)
          {
            if (j == origin.first)
              continue;

            const auto pair =
              std::make_pair(j, p_j.distance_square(origin.second));

            if (nearest.size() < N)
              {
                nearest.push_back(pair);

                if (nearest.size() == N)
                  std::make_heap(nearest.begin(), nearest.end(), is_closer);
              }
            else if (!is_closer(nearest.front(), pair))
              {
                std::pop_heap(nearest.begin(), nearest.end(), is_closer);
                nearest.pop_back();

                nearest.push_back(pair);
                std::push_heap(nearest.begin(), nearest.end(), is_closer);
              }
          }

        std::sort_heap(nearest.begin(), nearest.end(), is_closer);

        return nearest;
      }
  }

  RBFInterpolationDoFs::RBFInterpolationDoFs(const std::string &subsection)
    : RBFInterpolation(subsection)
  {}

  void
  RBFInterpolationDoFs::setup_system(const DoFHandler<dim> &dof_handler_src,
                                     const DoFHandler<dim> &dof_handler_dst)
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Setup system");

    // The two DoF handlers must have the same number of components.
    Assert(dof_handler_src.get_fe().n_components() ==
             dof_handler_dst.get_fe().n_components(),
           ExcDimensionMismatch(dof_handler_src.get_fe().n_components(),
                                dof_handler_dst.get_fe().n_components()));

    const unsigned int n_components = dof_handler_src.get_fe().n_components();

    std::vector<PointMap> points_owned_src(n_components);
    std::vector<PointMap> points_owned_dst(n_components);

    for (unsigned int c = 0; c < n_components; ++c)
      {
        compute_points(dof_handler_src, points_owned_src[c], c);
        compute_points(dof_handler_dst, points_owned_dst[c], c);
      }

    setup_system_internal(points_owned_src, points_owned_dst);
  }

  void
  RBFInterpolationDoFs::compute_points(const DoFHandler<dim> &dof_handler,
                                       PointMap &             owned_points,
                                       const unsigned int &   component) const
  {
    ComponentMask mask(dof_handler.get_fe().n_components(), false);
    mask.set(component, true);

    const auto mapping =
      MeshHandler::get_linear_mapping(dof_handler.get_triangulation());

    std::map<types::global_dof_index, Point<dim>> points;
    DoFTools::map_dofs_to_support_points(*mapping, dof_handler, points, mask);

    // Build LabelledPoints out of the Points.
    IndexSet owned_dofs = dof_handler.locally_owned_dofs();
    for (const auto &i : points)
      {
        if (!owned_dofs.is_element(i.first))
          continue;

        owned_points[i.first] = LabelledPoint(i.second);
      }

    // Add labels if needed.
    if (!prm_map_id_label.empty())
      {
        const FiniteElement<dim> &           fe = dof_handler.get_fe();
        const unsigned int                   dofs_per_cell = fe.dofs_per_cell;
        std::vector<types::global_dof_index> dof_indices(dofs_per_cell);

        for (const auto &cell : dof_handler.active_cell_iterators())
          {
            // We also loop over ghost cells to make sure that labels for owned
            // points also account for material IDs of cells owned by other
            // processes.
            if (!cell->is_locally_owned() && !cell->is_ghost())
              continue;

            const auto label = prm_map_id_label.find(cell->material_id());

            if (label == prm_map_id_label.end())
              continue;

            cell->get_dof_indices(dof_indices);
            for (unsigned int i = 0; i < dofs_per_cell; ++i)
              if (fe.system_to_component_index(i).first == component &&
                  owned_dofs.is_element(dof_indices[i]))
                owned_points[dof_indices[i]].set_label(label->second);
          }
      }
  }

  template <class OutputType>
  RBFInterpolationQuadrature<OutputType>::RBFInterpolationQuadrature(
    const std::string &subsection)
    : RBFInterpolation(subsection)
  {}

  template <class OutputType>
  void
  RBFInterpolationQuadrature<OutputType>::setup_system(
    const unsigned int &   n_components_,
    const DoFHandler<dim> &dof_handler_src_,
    const Quadrature<dim> &quadrature_src,
    const DoFHandler<dim> &dof_handler_dst,
    const Quadrature<dim> &quadrature_dst)
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Setup system");

    n_components    = n_components_;
    dof_handler_src = &dof_handler_src_;
    n_q_src         = quadrature_src.size();

    // The interpolant operates on one component at a time: therefore, we only
    // store one point map for the source and destination meshes.
    std::vector<PointMap> points_owned_src(1);
    std::vector<PointMap> points_owned_dst(1);

    compute_points(dof_handler_src_, points_owned_src[0], quadrature_src);
    compute_points(dof_handler_dst, points_owned_dst[0], quadrature_dst);

    const unsigned int n_points_src = points_owned_src[0].size();
    const unsigned int n_points_dst = points_owned_dst[0].size();

    setup_system_internal(points_owned_src, points_owned_dst);

    // Compute the offset for current rank in the source vectors.
    const std::vector<unsigned int> n_points_src_per_rank =
      Utilities::MPI::all_gather(mpi_comm, n_points_src);
    offset_src = 0;
    for (unsigned int i = 0; i < mpi_rank; ++i)
      offset_src += n_points_src_per_rank[i];

    // Compute the offset for current rank in the destination vectors.
    const std::vector<unsigned int> n_points_dst_per_rank =
      Utilities::MPI::all_gather(mpi_comm, n_points_dst);
    offset_dst = 0;
    for (unsigned int i = 0; i < mpi_rank; ++i)
      offset_dst += n_points_dst_per_rank[i];

    // Setup facilities for quadrature evaluation.
    QuadratureEvaluationFEM<OutputType>::setup(dof_handler_dst, quadrature_dst);

    interpolated_vector.resize(n_components);
    for (auto &vec : interpolated_vector)
      vec.reinit(owned_points_dst, mpi_comm);
  }

  template <class OutputType>
  void
  RBFInterpolationQuadrature<OutputType>::compute_points(
    const DoFHandler<dim> &dof_handler,
    PointMap &             owned_points,
    const Quadrature<dim> &quadrature) const
  {
    FEValues<dim> fe_values(dof_handler.get_fe(),
                            quadrature,
                            update_quadrature_points);

    std::vector<LabelledPoint> owned_points_vector;
    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (!cell->is_locally_owned())
          continue;

        fe_values.reinit(cell);

        const auto label_it = prm_map_id_label.find(cell->material_id());
        const unsigned int label =
          label_it != prm_map_id_label.end() ? label_it->second : 0;

        for (unsigned int q = 0; q < quadrature.size(); ++q)
          owned_points_vector.emplace_back(fe_values.quadrature_point(q),
                                           label);
      }

    const unsigned int              n_owned_points = owned_points_vector.size();
    const std::vector<unsigned int> n_points =
      Utilities::MPI::all_gather(mpi_comm, n_owned_points);

    unsigned int offset = 0;
    for (unsigned int i = 0; i < mpi_rank; ++i)
      offset += n_points[i];

    for (unsigned int i = 0; i < owned_points_vector.size(); ++i)
      owned_points[offset + i] = owned_points_vector[i];
  }

  template <class OutputType>
  void
  RBFInterpolationQuadrature<OutputType>::interpolate(
    QuadratureEvaluationScalar &src)
  {
    // This only works if the interpolator has 1 component.
    Assert(n_components == 1, ExcDimensionMismatch(n_components, 1));

    src.init();

    LinAlg::MPI::Vector src_vector(owned_points_src, mpi_comm);

    unsigned int idx = 0;

    for (const auto &cell : dof_handler_src->active_cell_iterators())
      {
        if (!cell->is_locally_owned())
          continue;

        src.reinit(cell);

        for (unsigned int q = 0; q < n_q_src; ++q)
          {
            src_vector[offset_src + idx] = src(q);
            ++idx;
          }
      }

    interpolate_internal(interpolated_vector[0], src_vector, false);
  }

  template <class OutputType>
  void
  RBFInterpolationQuadrature<OutputType>::interpolate(
    QuadratureEvaluation<Vector<double>> &src)
  {
    src.init();

    std::vector<LinAlg::MPI::Vector> src_vectors;
    for (unsigned int d = 0; d < n_components; ++d)
      src_vectors.emplace_back(owned_points_src, mpi_comm);

    unsigned int   idx = 0;
    Vector<double> src_loc(n_components);

    for (const auto &cell : dof_handler_src->active_cell_iterators())
      {
        if (!cell->is_locally_owned())
          continue;

        src.reinit(cell);

        for (unsigned int q = 0; q < n_q_src; ++q)
          {
            src_loc = src(q);

            Assert(src_loc.size() == n_components,
                   ExcDimensionMismatch(src_loc.size(), n_components));

            for (unsigned int d = 0; d < n_components; ++d)
              src_vectors[d][offset_src + idx] = src_loc[d];

            ++idx;
          }
      }

    for (unsigned int d = 0; d < n_components; ++d)
      {
        pcout << "\tcomponent " << d << std::flush;
        interpolate_internal(interpolated_vector[d], src_vectors[d], false);
      }
  }

  template <class OutputType>
  void
  RBFInterpolationQuadrature<OutputType>::post_init_callback()
  {
    cell_offset   = 0;
    reinit_called = false;
  }

  template <class OutputType>
  void
  RBFInterpolationQuadrature<OutputType>::post_reinit_callback(
    const DoFHandler<dim>::active_cell_iterator & /*cell_other*/)
  {
    if (reinit_called)
      cell_offset += this->fe_values->get_quadrature().size();

    reinit_called = true;
  }

  /// Explicit instantiation.
  template class RBFInterpolationQuadrature<double>;

  /// Explicit instantiation.
  template class RBFInterpolationQuadrature<Vector<double>>;

  /// Explicit instantiation.
  template class RBFInterpolationQuadrature<Tensor<2, dim, double>>;
} // namespace lifex::utils