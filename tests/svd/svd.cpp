/********************************************************************************
  Copyright (C) 2019 - 2022 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/core_model.hpp"
#include "source/init.hpp"

#include "source/numerics/numbers.hpp"

#include <random>

namespace lifex::tests
{
  /// Test class for Bessel functions.
  class TestSVD : public CoreModel
  {
  public:
    /// Constructor.
    TestSVD(const std::string &subsection)
      : CoreModel(subsection)
    {}

    /// Declare parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override
    {
      params.enter_subsection(prm_subsection_path);
      params.declare_entry(
        "Number of tests",
        "100",
        Patterns::Integer(1),
        "Number of tests of the SVD factorization with random matrices.");

      params.declare_entry("Tolerance",
                           "1e-14",
                           Patterns::Double(0),
                           "Tolerance used to determine if the test passes.");
      params.leave_subsection();
    }

    /// Parse parameters.
    virtual void
    parse_parameters(ParamHandler &params) override
    {
      params.parse();

      params.enter_subsection(prm_subsection_path);
      prm_n_tests   = params.get_integer("Number of tests");
      prm_tolerance = params.get_double("Tolerance");
      params.leave_subsection();
    }

    /// Run the test.
    virtual void
    run() override
    {
      // We initialize the random engine with a fixed seed, to ensure
      // reproducibility.
      std::default_random_engine             engine(1);
      std::uniform_real_distribution<double> rand(-1, 1);

      Tensor<2, dim>          A, U, VT, A_recomposed;
      std::array<double, dim> S;

      for (unsigned int n = 0; n < prm_n_tests; ++n)
        {
          for (unsigned int i = 0; i < dim; ++i)
            for (unsigned int j = 0; j < dim; ++j)
              A[i][j] = rand(engine);

          {
            TimerOutput::Scope timer_section(timer_output,
                                             prm_subsection_path +
                                               " / Compute SVD factorization");
            std::tie(U, S, VT) = utils::aligned_svd(A);
          }

          A_recomposed = 0.0;
          for (unsigned int i = 0; i < dim; ++i)
            for (unsigned int j = 0; j < dim; ++j)
              for (unsigned int k = 0; k < dim; ++k)
                A_recomposed[i][j] += U[i][k] * S[k] * VT[k][j];

          const double error_frobenius = (A - A_recomposed).norm();
          AssertThrow(error_frobenius < prm_tolerance,
                      ExcMessage("Error in SVD factorization computation"));
        }
    }

  protected:
    /// Number of tests.
    unsigned int prm_n_tests;

    /// Test tolerance.
    double prm_tolerance;
  };
}; // namespace lifex::tests

/// Run SVD test.
int
main(int argc, char **argv)
{
  lifex::lifex_init lifex_initializer(argc, argv, 1);

  try
    {
      lifex::tests::TestSVD test("Test SVD");
      test.main_run_generate();
    }
  LIFEX_CATCH_EXC();

  return EXIT_SUCCESS;
}
