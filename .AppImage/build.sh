#!/bin/bash
## ---------------------------------------------------------------------
## Copyright (C) 2021 - 2022 by the lifex authors.
##
## This file is part of lifex.
##
## lifex is free software; you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## lifex is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with lifex.  If not, see <http://www.gnu.org/licenses/>.
## ---------------------------------------------------------------------

# Author: Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.

#
# This script builds lifex and generates its corresponding AppImage using
# linuxdeploy.
#

set -e

LIFEX_DIR="$(dirname $(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd))"

cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=/usr \
      ${LIFEX_DIR}

make -j$(( $(nproc) / 2 )) \
     install \
     DESTDIR=AppDir

# Get linuxdeploy.
LINUXDEPLOY_EXEC=linuxdeploy-x86_64.AppImage
if [[ ! -f "${LINUXDEPLOY_EXEC}" ]]
then
   wget https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/${LINUXDEPLOY_EXEC}

   chmod +x "${LINUXDEPLOY_EXEC}"
   ./${LINUXDEPLOY_EXEC} --appimage-extract
fi

# AppRun is just a symbolic link to the main executable
# (a relative path must be used for portability).
ln -sfv \
   usr/bin/lifexrun \
   ${LIFEX_DIR}/build/AppDir/AppRun

# Generate AppImage.
VERSION=$(head -n1 ${LIFEX_DIR}/VERSION_lifex) \
LD_LIBRARY_PATH+=:$(pwd)/AppDir/usr/lib:${VTK_DIR}:${DEAL_II_DIR}/lib:${BOOST_DIR}/lib:${P4EST_DIR}/FAST/lib:${TRILINOS_DIR}/lib:${ADOLC_DIR}/lib64:${ARPACK_DIR}/lib:${HDF5_DIR}/lib:${SLEPC_DIR}/lib:${PETSC_DIR}/lib:${PARMETIS_DIR}/lib \
squashfs-root/usr/bin/linuxdeploy \
  --appdir AppDir/ \
  --desktop-file ${LIFEX_DIR}/.AppImage/lifex.desktop \
  --icon-file ${LIFEX_DIR}/.AppImage/lifex_icon.png \
  --output appimage
